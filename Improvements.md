### Some things that can be improved

- Add lazy loading for images outside of viewport
- Fetch images based on size of container and resolution of the device
- Break into smaller components when needed
- Change use of hardcoded text to localised key
- Add snapshot tests for items
- Ensure cross browser compatibility for a much wider range of browsers via some form of visual regression tests