import React from "react"
import userIcon from "./images/user.jpg"
import userIcon2x from "./images/user@2x.jpg"
import userIcon3x from "./images/user@3x.jpg"
import "./App.css"
import data from "./restaurants.json"

const App = props => {
  return (
    <div className="app">
      <header className="header">
        <a href="/" className="logo">
          Deliveroo
        </a>
        <a href="#" className="user">
          <img
            srcSet={`
              ${userIcon}, 
              ${userIcon2x} 2x, 
              ${userIcon3x} 3x
            `}
            src={userIcon2x}
            className="user-icon"
            alt="User account"
          />
          <span className="user-name">{props.name}</span>
        </a>
      </header>
      <div className="location">
        <div className="current-location">
          <span className="location-label">Location</span>
          <span className="location-value">{data.neighborhood}</span>
        </div>
        <button className="location-change">Change Location</button>
      </div>
      <div className="body">
        <span className="search-count">
          {data.restaurants.length} Restaurant
          {data.restaurants.length > 1 ? "s" : ""}
        </span>
        <div className="search-results">
          {data.restaurants.map(item => {
            return (
              <a href={item.url} className="item">
                <img src={item.image} alt={item.name} className="item-image" />
                <span className="item-title">{item.name}</span>
                <span className="item-subtitle">
                  {item.tags.join(" · ")} ·{" "}
                  {Array(item.price)
                    .fill()
                    .map(_ => "£")}
                </span>
              </a>
            )
          })}
        </div>
      </div>
    </div>
  )
}

export default App
